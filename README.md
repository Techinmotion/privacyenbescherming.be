# 2 Quick Tips on Preventing Online Identity Theft #

Hackers are always coming up with new and improved ways of getting their hands on your personal information online. This is why it is important to educate yourself on online security and what hackers are capable of. 

For a start, you should:

### Be Wary of Phishing Scams ###

One of the main methods used by hackers to get your personal information is via phishing scams. They might have set up a fake website that looks identical to the legitimate original. They might send you fake emails pretending to be from somewhere official, asking for you to click a link or provide important information. 

Always learn about the latest phishing scams and be very wary when any email asks you to click a link. If in doubt, contact the original website immediately.

### Check That Websites are Legitimate ###

Many people just assume that all websites are legitimate and therefore safe to complete financial transactions on. This is not true, as a ‘HTTP’ site does not use SSL encryption like a ‘HTTPS’ site does. Always check the address of a website to ensure that it is HTTPS before making any transactions. 

To add further security, use a VPN too. While a VPN for Privacy, they also offer encryption. THis is another welcome layer of protection.

### Key Takeaway ###

As long as you have an antivirus, a firewall, and a VPN installed on your system and you are well educated on phishing scams and the like, you should be able to stay one step ahead of hackers. Just make sure that you educate your family too, as identity theft can ruin peoples lives.

[https://privacyenbescherming.be](https://privacyenbescherming.be)